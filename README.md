# SwtorMeter

A very basic parser for SWTOR logs.

## Install

Download the archive and extract to any folder you like.

- [Windows 64 bits](https://gitlab.com/mguimard/swtormeter/-/jobs/artifacts/main/download?job=Windows%2064)
- [Linux 64 bits AppImage](https://gitlab.com/mguimard/swtormeter/-/jobs/artifacts/main/download?job=Linux%2064%20AppImage)


