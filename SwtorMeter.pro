QT += core gui websockets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0



SOURCES += \
    src/main.cpp \
    src/entity/combat.cpp \
    src/entity/playercolorpool.cpp \
    src/entity/playerdisciplinepool.cpp \
    src/ui/main/overlayconfigwidget.cpp \
    src/ui/overlay/overlayapp.cpp \
    src/ui/overlay/overlaytabledelegate.cpp \
    src/parser/oneshotparserthread.cpp \
    src/parser/parser.cpp \
    src/parser/realtimeparserthread.cpp \
    src/ui/main/combatlistwidget.cpp \
    src/ui/main/combattabledelegate.cpp \
    src/ui/main/combatwidget.cpp \
    src/ui/main/mainwindow.cpp \
    src/ui/overlay/overlay.cpp \
    src/util/logger.cpp \
    src/util/settings.cpp

HEADERS += \
    src/data/constants.h \
    src/entity/combat.h \
    src/entity/event.h \
    src/entity/playercolorpool.h \
    src/entity/playerdisciplinepool.h \
    src/entity/playerstats.h \
    src/entity/role.h \
    src/ui/main/overlayconfigwidget.h \
    src/ui/overlay/overlayapp.h \
    src/ui/overlay/overlaytabledelegate.h \
    src/parser/oneshotparserthread.h \
    src/parser/parser.h \
    src/parser/realtimeparserthread.h \
    src/ui/main/apptheme.h \
    src/ui/main/combatlistwidget.h \
    src/ui/main/combattabledelegate.h \
    src/ui/main/combatwidget.h \
    src/ui/main/mainwindow.h \
    src/ui/overlay/overlay.h \
    src/util/logger.h \
    src/util/numberutils.h \
    src/util/overlay_config.h \
    src/util/settings.h

FORMS += \
    src/ui/main/overlayconfigwidget.ui \
    src/ui/main/combatlistwidget.ui \
    src/ui/main/combatwidget.ui \
    src/ui/main/mainwindow.ui \
    src/ui/overlay/overlay.ui

contains(QMAKE_PLATFORM, linux) {
    LIBS += -lLayerShellQtInterface
}


# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    src/res/resources.qrc
