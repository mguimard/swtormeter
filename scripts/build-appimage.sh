#!/bin/bash

set -x
set -e

TEMP_BASE=/tmp
BUILD_DIR=$(mktemp -d -p "$TEMP_BASE" appimage-build-XXXXXX)
ARCH=x86_64
TARGET=SwtorMeter

cleanup () {
    if [ -d "$BUILD_DIR" ]; then
        rm -rf "$BUILD_DIR"
    fi
}

trap cleanup EXIT

REPO_ROOT=$(readlink -f $(dirname $(dirname $0)))
OLD_CWD=$(readlink -f .)

pushd "$BUILD_DIR"

qmake "$REPO_ROOT"

make -j$(nproc) TARGET="$TARGET"
make install INSTALL_ROOT=AppDir

export QML_SOURCES_PATHS="$REPO_ROOT"/src

chmod +x "$REPO_ROOT"/scripts/tools/*

"$REPO_ROOT"/scripts/tools/linuxdeploy-"$ARCH".AppImage --appdir AppDir -e "$TARGET" -i "$REPO_ROOT"/res/SwtorMeter.png -d "$REPO_ROOT"/SwtorMeter.desktop
"$REPO_ROOT"/scripts/tools/linuxdeploy-plugin-qt-"$ARCH".AppImage --appdir AppDir
"$REPO_ROOT"/scripts/tools/linuxdeploy-"$ARCH".AppImage --appdir AppDir --output appimage

mv -v "$TARGET"*.AppImage "$OLD_CWD"
