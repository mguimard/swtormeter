#include "src/entity/combat.h"
#include "src/data/constants.h"
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include "src/util/logger.h"

#define ONE_DAY_MS 86400000

Combat::Combat(){}

bool Combat::empty()
{
    return events.empty();
}

QString Combat::displayTime() const
{
    QString text;

    text.append(start_time.toString());
    text.append(" - ");
    text.append(end_time.toString());

    return text;
}

QString Combat::targetNames() const
{
    QString text;

    for(const QString& target: targets)
    {
        text +=  ", " +target;
    }

    text = text.mid(1).trimmed();

    return text;
}

QString Combat::displayDuration() const
{
    QString text;

    int seconds = (duration / 1000) % 60;
    int minutes = (duration / 1000) / 60;

    QString seconds_text = QStringLiteral("%1").arg(seconds, 2, 10, QLatin1Char('0'));

    text.append(QString::number(minutes));
    text.append(":");
    text.append(seconds_text);

    return text;
}

QString Combat::displayTitle() const
{
    QString text;

    if(targets.size() > 1)
    {
        text.append(QString(targets.begin()->data()));
        text.append(" and ");
        text.append(QString::number(targets.size() - 1));
        text.append(" other");
    }
    else
    {
        text.append(targetNames());
    }

    return text;
}

void Combat::append(Event e)
{
    if(!e.source.isEmpty() && !stats.contains(e.source))
    {
        stats.insert(e.source, {0,0,0});
    }

    if(e.type == constants::ENTER_COMBAT)
    {
        start_time = QTime::fromString(e.time);
    }
    else if(e.type == constants::DAMAGE)
    {
        stats[e.source].damages += e.value.toLongLong();
    }
    else if(e.type == constants::HEAL)
    {
        stats[e.source].heals += e.value.toLongLong();
    }
    else if(e.type == constants::ABILITY)
    {
        stats[e.source].abilities++;
    }
    else if (e.type.isEmpty())
    {
        // ignore
        return;
    }

    // Consider latest entry as end
    end_time = QTime::fromString(e.time);

    // handle the next day case
    if(end_time.hour() < start_time.hour())
    {
        duration = ONE_DAY_MS + start_time.msecsTo(end_time);
    }
    else
    {
        duration = start_time.msecsTo(end_time);

    }

    if(!e.target.isEmpty() && e.target != "=" && !e.target.startsWith("@"))
    {
        targets.insert(e.target);
    }

    if(!boss_combat && constants::BOSS_IDS.contains(e.target_id))
    {
        boss_combat = true;
    }

    events.append(e);
}

QJsonDocument Combat::toJSON()
{
    QJsonObject o;

    o["duration"] = QString::number(duration);

    QJsonObject stats_json;

    for (const auto& it : stats.toStdMap())
    {
        QJsonArray stats_values =
        {
            QString::number(it.second.damages),
            QString::number(it.second.heals),
            QString::number(it.second.abilities)
        };
        stats_json[it.first] = stats_values;
    }

    o["stats"] = stats_json;

    return QJsonDocument(o);
}

Combat* Combat::fromJSON(QJsonDocument doc)
{    
    Combat* combat = new Combat();

    combat->duration = doc["duration"].toString().toLongLong();

    QJsonObject stats = doc["stats"].toObject();

    foreach(const QString& key, stats.keys())
    {
        QJsonArray value = stats.value(key).toArray();
        PlayerStats ps;
        ps.damages = value.at(0).toString().toLongLong();
        ps.heals = value.at(1).toString().toLongLong();
        ps.abilities = value.at(2).toString().toLongLong();
        combat->stats[key] = ps;
    }

    return combat;
}

