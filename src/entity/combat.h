#ifndef COMBAT_H
#define COMBAT_H

#include <QString>
#include <QList>
#include <QMap>
#include <QSet>
#include <QTime>
#include <QJsonDocument>
#include <QtGlobal>

#include "src/entity/event.h"
#include "src/entity/playerstats.h"

class Combat
{
public:
    Combat();
    void append(Event e);
    QString displayTitle() const;
    QString displayTime() const;
    QString targetNames() const;
    QString displayDuration() const;
    bool empty();

    QSet<QString> targets;
    QList<Event> events;
    QMap<QString,PlayerStats> stats;
    QTime start_time;
    QTime end_time;
    quint64 duration;
    bool boss_combat = false;

    QJsonDocument toJSON();
    static Combat* fromJSON(QJsonDocument);
};

#endif // COMBAT_H
