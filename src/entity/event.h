#ifndef EVENT_H
#define EVENT_H

#include <QString>
#include <QJsonObject>
#include <QJsonDocument>

struct Event
{
    QString time;
    QString type;
    QString target;
    QString target_id;
    QString source;
    QString value;

    QString toString() const
    {
        return time + ", " + type + ", " + target + ", " + source + ", " + value;
    }
};

#endif // EVENT_H
