#include "src/entity/playercolorpool.h"

PlayerColorPool* PlayerColorPool::instance;

PlayerColorPool::PlayerColorPool(){}

QColor PlayerColorPool::get(QString key, int alpha)
{
    if(!instance)
    {
        instance = new PlayerColorPool();
    }

    if(!instance->pool.contains(key))
    {
        instance->generate(key);
    }

    QColor color = instance->pool[key];
    color.setAlpha(alpha);

    return color;
}

void PlayerColorPool::generate(QString key)
{
    int count = pool.size() + 1;
    int step = 360 / count;

    QList<QString> keys = pool.keys();
    keys.append(key);

    pool.clear();

    int h = 0;

    for (QString key : keys)
    {
        pool[key] = QColor::fromHsv(h, DEFAULT_HSV_SATURATION, DEFAULT_HSV_VALUE);
        h += step;
    }
}



