#ifndef PLAYERCOLORPOOL_H
#define PLAYERCOLORPOOL_H

#include <QColor>
#include <QString>
#include <QMap>

#define DEFAULT_ALPHA 150
#define DEFAULT_HSV_SATURATION 255
#define DEFAULT_HSV_VALUE 255

class PlayerColorPool
{
public:
    static QColor get(QString, int alpha = DEFAULT_ALPHA);

private:
    PlayerColorPool();
    QMap<QString,QColor> pool;
    void generate(QString);

    static PlayerColorPool* instance;
};

#endif // PLAYERCOLORPOOL_H
