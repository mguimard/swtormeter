#include "src/entity/playerdisciplinepool.h"
#include "src/entity/role.h"
#include "src/util/logger.h"

PlayerDisciplinePool* PlayerDisciplinePool::instance;

PlayerDisciplinePool::PlayerDisciplinePool() {}

Role PlayerDisciplinePool::get(QString key)
{
    if(!instance)
    {
        instance = new PlayerDisciplinePool();
    }

    if(!instance->pool.contains(key))
    {
        return Unknown;
    }

    return instance->pool[key];
}

void PlayerDisciplinePool::add(QString key, QString discipline_id)
{
    if(!instance)
    {
        instance = new PlayerDisciplinePool();
    }

    instance->pool[key] = GetRoleFromDisciplineID(discipline_id);
}

Role PlayerDisciplinePool::GetRoleFromDisciplineID(QString id)
{
    for (auto it = constants::ROLES_FOR_DISCIPLINE.keyValueBegin(); it != constants::ROLES_FOR_DISCIPLINE.keyValueEnd(); ++it)
    {
        if(it->second.contains(id))
        {
            return it->first;
        }
    }

    logger::log("PlayerDisciplinePool", "No role for " + id);
    return Unknown;

}
