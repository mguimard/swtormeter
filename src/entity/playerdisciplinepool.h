#ifndef PLAYERDISCIPLINEPOOL_H
#define PLAYERDISCIPLINEPOOL_H

#include <QMap>
#include <QString>
#include "src/data/constants.h"

class PlayerDisciplinePool
{
public:
    static Role get(QString);
    static void add(QString, QString);

private:
    PlayerDisciplinePool();
    QMap<QString,Role> pool;
    static PlayerDisciplinePool* instance;

    static Role GetRoleFromDisciplineID(QString);
};

#endif // PLAYERDISCIPLINEPOOL_H
