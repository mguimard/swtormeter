#ifndef PLAYERSTATS_H
#define PLAYERSTATS_H

#include <QtGlobal>

struct PlayerStats
{
    quint64 damages;
    quint64 heals;
    quint64 abilities;
};

#endif // PLAYERSTATS_H
