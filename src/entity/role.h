#ifndef ROLE_H
#define ROLE_H

enum Role
{
    Tank    = 0,
    Heal    = 1,
    Dps     = 2,
    Unknown = 3
};

#endif // ROLE_H
