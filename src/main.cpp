#include <QApplication>

#include "src/ui/main/mainwindow.h"
#include "src/ui/main/apptheme.h"
#include "src/ui/overlay/overlayapp.h"
#include "src/util/logger.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QCoreApplication::setApplicationName("SwtorMeter");
    QCoreApplication::setOrganizationDomain("SwtorMeterDomain");
    QCoreApplication::setOrganizationName("SwtorMeter");
    QCoreApplication::setApplicationVersion("0.2");

    QStringList arguments = QCoreApplication::arguments();

    if(arguments.size() == 1)
    {
        logger::log("main", "Creating main window");

        AppTheme::SetTheme();

        MainWindow w;
        w.show();

        return a.exec();
    }
    else if(arguments.at(1) == "overlay")
    {
        logger::log("main", "Creating overlay window");

        OverlayApp overlay_app;
        overlay_app.run();

        return a.exec();
    }
}
