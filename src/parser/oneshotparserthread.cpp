#include "src/parser/oneshotparserthread.h"
#include "src/util/logger.h"
#include "src/data/constants.h"
#include <QFile>
#include <QTextStream>

#include <chrono>
using namespace std::chrono;


OneShotParserThread::OneShotParserThread(QString file_name): file_name(file_name){}

void OneShotParserThread::run()
{
    logger::log(typeid(this).name(), "Starting parsing of " + file_name);

    auto start = high_resolution_clock::now();

    QList<Combat*> combats;

    Combat* current_combat = nullptr;

    QFile inputFile(file_name);

    if (inputFile.open(QIODevice::ReadOnly))
    {
        QTextStream in(&inputFile);
        in.setCodec("Latin1");

        while (!in.atEnd())
        {
            QString line = in.readLine();
            Event e = p.parseLine(line);

            if(e.type == constants::ENTER_COMBAT)
            {
                // Combat already ongoing but got a new combat event
                if(current_combat)
                {
                    combats.append(current_combat);
                }

                current_combat = new Combat();
                current_combat->append(e);
            }
            else if(e.type == constants::EXIT_COMBAT)
            {
                if(current_combat && current_combat->targets.size() > 0)
                {
                    current_combat->append(e);
                    combats.append(current_combat);
                }

                current_combat = nullptr;
            }
            else if(current_combat && e.type != constants::UNKNOWN && !e.type.isEmpty())
            {
                current_combat->append(e);
            }
        }

        inputFile.close();
    }

    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start);

    logger::log(typeid(this).name(), QString::number(combats.size()) + " combats parsed in " + QString::number(duration.count()/1000.0) + " ms");

    emit resultReady(combats);
}
