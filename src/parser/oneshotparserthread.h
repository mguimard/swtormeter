#ifndef ONESHOTPARSERTHREAD_H
#define ONESHOTPARSERTHREAD_H

#include <QThread>
#include "src/entity/combat.h"
#include "src/parser/parser.h"

class OneShotParserThread : public QThread
{
    Q_OBJECT
    void run() override;

public:
    OneShotParserThread(QString);

signals:
    void resultReady(const QList<Combat*>);

private:
    QString file_name;
    Parser p;
};

#endif // ONESHOTPARSERTHREAD_H
