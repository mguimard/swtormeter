#include "src/parser/parser.h"
#include <QRegularExpressionMatch>
#include "src/util/logger.h"
#include "src/entity/playerdisciplinepool.h"

Parser::Parser(){}

Event Parser::parseLine(QString line)
{
    Event event;

    QRegularExpressionMatch main_match = main_re.match(line);

    if (main_match.hasMatch())
    {
        event.time = main_match.captured(1);

        QString source = main_match.captured(2);
        QString target = main_match.captured(3);
        QString detail = main_match.captured(4);
        QString event_data = main_match.captured(5);
        QString rest = main_match.captured(6).trimmed();

        if(event_data.contains(constants::ENTER_COMBAT))
        {
            event.type = constants::ENTER_COMBAT;
        }
        else if(event_data.contains(constants::EXIT_COMBAT))
        {
            event.type = constants::EXIT_COMBAT;
        }
        else if(event_data.contains(constants::ABILITY) && source.startsWith("@"))
        {
            event.type = constants::ABILITY;
            event.source = ((source.split("|")[0]).split("#")[0]).mid(1).trimmed();
        }
        else if(event_data.contains(constants::DAMAGE) && source.startsWith("@") && target != "=" && !target.startsWith("@"))
        {
            event.type = constants::DAMAGE;
            event.source = ((source.split("|")[0]).split("#")[0]).mid(1).trimmed();
            event.target = (target.split("{")[0]).trimmed();
            event.target_id = ((target.split("{")[1]).split("}")[0]).trimmed();

            QRegularExpressionMatch rest_match = rest_re.match(rest);

            if (rest_match.hasMatch())
            {
                event.value =  rest_match.captured(1);
            }
        }
        else if(event_data.contains(constants::HEAL) && source.startsWith("@"))
        {
            event.type = constants::HEAL;
            event.source = ((source.split("|")[0]).split("#")[0]).mid(1).trimmed();

            QRegularExpressionMatch value_match = rest_re.match(rest);

            if (value_match.hasMatch())
            {
                event.value = value_match.captured(1);
            }
        }
        else if(event_data.contains(constants::DISCIPLINE_CHANGED))
        {
            event.source = ((source.split("|")[0]).split("#")[0]).mid(1).trimmed();
            event.type = constants::DISCIPLINE_CHANGED;

            QRegularExpressionMatch disc_match = disc_re.match(event_data);

            if (disc_match.hasMatch())
            {
                QString id = disc_match.captured(1);
                PlayerDisciplinePool::add(event.source, id);                
            }
        }
        else
        {
            event.type = constants::UNKNOWN;
        }
    }
    else
    {
        logger::log(typeid(this).name(), "INCOMPLETE line: " + line);
        event.type = constants::INCOMPLETE;
    }

    return event;
}


