#ifndef PARSER_H
#define PARSER_H

#include <QString>
#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include "src/entity/event.h"

//  [timestamp] [source] [target] [ability] [event | effect] (value) <threat>
#define MAIN_RE "^\\[(\\d\\d:\\d\\d:\\d\\d\\.\\d\\d\\d)\\] \\[(.*)\\] \\[(.*)\\] \\[(.*)\\] \\[(.*)\\](.*)$"
#define REST_RE "^\\(([\\d]+).*\\)"
#define DISC_RE ".* \\{836045448953665\\}: .* {.*}/.* {(.*)}"

class Parser
{
public:
    Parser();
    Event parseLine(const QString);
private:
    QRegularExpression main_re = QRegularExpression(MAIN_RE);
    QRegularExpression rest_re = QRegularExpression(REST_RE);
    QRegularExpression disc_re = QRegularExpression(DISC_RE);
};


#endif // PARSER_H

