#include "src/parser/realtimeparserthread.h"
#include "src/util/logger.h"
#include "src/parser/parser.h"
#include "src/data/constants.h"
#include <QDir>
#include <QFileSystemWatcher>
#include <QTextStream>

RealTimeParserThread::RealTimeParserThread(QString folder_path): folder_path(folder_path){}

void RealTimeParserThread::Stop()
{
    running = false;
    wait();
}

void RealTimeParserThread::run()
{
    // subscribe to folder events (add/rename/remove)
    QFileSystemWatcher watcher;
    watcher.addPath(folder_path);

    connect(&watcher, &QFileSystemWatcher::directoryChanged, this, [=](){
        logger::log(typeid(this).name(), "Folder change event");

        // todo...
    });

    // find the latest log file
    QDir directory(folder_path);
    QString current_file;

    logger::log(typeid(this).name(), "Listing folder...");
    QStringList files = directory.entryList(QDir::Files, QDir::Time);

    if(files.size() > 0)
    {
        current_file = files[0];

        logger::log(typeid(this).name(), "Latest file: " + current_file);

        QFile inputFile(folder_path + QDir::separator() + current_file);

        if (inputFile.open(QIODevice::ReadOnly))
        {
            logger::log(typeid(this).name(), "file opened");

            QTextStream in(&inputFile);
            in.setCodec("Latin1");

            bool real_time_ready = false;
            Combat* current_combat = nullptr;
            QList<Combat*> combats;

            while(running)
            {
                bool real_time_has_data = false;

                while(!in.atEnd())
                {
                    QString data = in.readLine();
                    Event e = p.parseLine(data);

                    // Real time parsing sometimes fails as it reads before the whole line is written.
                    // Seek back at position before in.readLine() was called and wait a bit.

                    if(e.type == constants::INCOMPLETE)
                    {
                        QThread::msleep(200);
                        in.seek(in.pos() - data.size());
                        continue;
                    }

                    if(e.type == constants::UNKNOWN)
                    {
                        continue;
                    }

                    if(!real_time_ready)
                    {
                        if(e.type == constants::ENTER_COMBAT)
                        {
                            // Combat already ongoing but got a new combat event
                            if(current_combat)
                            {
                                combats.append(current_combat);
                            }

                            current_combat = new Combat();
                            current_combat->append(e);
                        }
                        else if(e.type == constants::EXIT_COMBAT)
                        {
                            if(current_combat && current_combat->targets.size() > 0)
                            {
                                current_combat->append(e);
                                combats.append(current_combat);
                            }

                            current_combat = nullptr;
                        }
                        else if(current_combat && !e.type.isEmpty())
                        {
                            current_combat->append(e);
                        }
                    }
                    else
                    {
                        if(e.type == constants::ENTER_COMBAT)
                        {
                            // Combat already ongoing but got a new combat event
                            if(current_combat)
                            {
                                combats.append(current_combat);
                            }

                            current_combat = new Combat();
                            current_combat->append(e);
                            emit combat_start(current_combat);
                        }
                        else if(e.type == constants::EXIT_COMBAT)
                        {
                            if(current_combat && current_combat->targets.size() > 0)
                            {
                                current_combat->append(e);
                                combats.append(current_combat);
                            }

                            current_combat = nullptr;

                            emit combat_end();
                        }
                        else if(current_combat && !e.type.isEmpty())
                        {
                            if(current_combat)
                            {
                                current_combat->append(e);
                            }

                            if(real_time_ready)
                            {
                                real_time_has_data = true;
                                //emit combat_events();
                            }
                        }
                    }
                }

                if(real_time_has_data)
                {
                    emit combat_events();
                }

                if(!real_time_ready)
                {
                    emit combats_parsed(combats);
                }

                real_time_ready = true;

                QThread::msleep(100);
            }

            inputFile.close();
        }
    }
}

