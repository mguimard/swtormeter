#ifndef REALTIMEPARSERTHREAD_H
#define REALTIMEPARSERTHREAD_H

#include <QThread>
#include "src/entity/combat.h"
#include "src/parser/parser.h"

class RealTimeParserThread : public QThread
{
    Q_OBJECT
    void run() override;

public:
    RealTimeParserThread(QString);
    void Stop();

signals:
    void combats_parsed(const QList<Combat*>);
    void combat_start(Combat*);
    void combat_events();
    void combat_end();

private:
    bool running = true;
    QString folder_path;
    Parser p;
};

#endif // REALTIMEPARSERTHREAD_H
