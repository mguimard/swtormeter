#ifndef APPTHEME_H
#define APPTHEME_H

#include <QApplication>
#include <QStyleFactory>
#include <QFont>
#include <QPalette>

class AppTheme
{
public:
    static void SetTheme()
    {
        qApp->setStyle(QStyleFactory::create("Fusion"));

        QFont defaultFont = QApplication::font();
        defaultFont.setPointSize(defaultFont.pointSize()+2);
        qApp->setFont(defaultFont);

        QPalette p;
        p.setColor(QPalette::Window,QColor(53,53,53));
        p.setColor(QPalette::WindowText,Qt::white);
        p.setColor(QPalette::Disabled,QPalette::WindowText,QColor(127,127,127));
        p.setColor(QPalette::Base,QColor(42,42,42));
        p.setColor(QPalette::AlternateBase,QColor(66,66,66));
        p.setColor(QPalette::ToolTipBase,Qt::white);
        p.setColor(QPalette::ToolTipText,Qt::white);
        p.setColor(QPalette::Text,Qt::white);
        p.setColor(QPalette::Disabled,QPalette::Text,QColor(127,127,127));
        p.setColor(QPalette::Dark,QColor(35,35,35));
        p.setColor(QPalette::Shadow,QColor(20,20,20));
        p.setColor(QPalette::Button,QColor(53,53,53));
        p.setColor(QPalette::ButtonText,Qt::white);
        p.setColor(QPalette::Disabled,QPalette::ButtonText,QColor(127,127,127));
        p.setColor(QPalette::BrightText,Qt::red);
        p.setColor(QPalette::Link,QColor(42,130,218));
        p.setColor(QPalette::Highlight,QColor(42,130,218));
        p.setColor(QPalette::Disabled,QPalette::Highlight,QColor(80,80,80));
        p.setColor(QPalette::HighlightedText,Qt::white);
        p.setColor(QPalette::Disabled,QPalette::HighlightedText,QColor(127,127,127));

        qApp->setPalette(p);
    }
};

#endif // APPTHEME_H
