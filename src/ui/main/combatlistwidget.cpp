#include "src/ui/main/combatlistwidget.h"
#include "ui_combatlistwidget.h"

CombatListWidget::CombatListWidget(Combat* combat, QWidget *parent) : QWidget(parent), ui(new Ui::CombatListWidget), combat(combat)
{
    ui->setupUi(this);
    Update();
}

CombatListWidget::~CombatListWidget()
{
    delete ui;
}

void CombatListWidget::Update()
{
    ui->targets->setText(combat->displayTitle());
    ui->duration->setText(combat->displayDuration());

    if(combat->boss_combat)
    {
        setStyleSheet("*{color: #f7d34c;}");
    }
}
