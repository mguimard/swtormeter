#ifndef COMBATLISTWIDGET_H
#define COMBATLISTWIDGET_H

#include <QWidget>
#include "src/entity/combat.h"

namespace Ui {
class CombatListWidget;
}

class CombatListWidget : public QWidget
{
    Q_OBJECT

public:
    explicit CombatListWidget(Combat* c, QWidget *parent = nullptr);
    ~CombatListWidget();

    void Update();

private:
    Ui::CombatListWidget *ui;
    Combat* combat;
};

#endif // COMBATLISTWIDGET_H
