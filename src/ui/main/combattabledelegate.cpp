#include "src/ui/main/combattabledelegate.h"
#include "src/util/numberutils.h"
#include "src/entity/playercolorpool.h"
#include "src/entity/playerdisciplinepool.h"

#include <QApplication>

void CombatTableDelegate::SetValues(quint64 d, quint64 h, qreal a)
{
    max_damage = d;
    max_heal = h;
    max_apm = a;
}

void CombatTableDelegate::initStyleOption(QStyleOptionViewItem *option, const QModelIndex &index) const
{
    QStyledItemDelegate::initStyleOption(option, index);

    int col = index.column();

    if(col == NAME_COL)
    {
        QBrush normalText = option->palette.brush(QPalette::ColorGroup::Normal, QPalette::ColorRole::Text);
        option->palette.setBrush(QPalette::ColorGroup::Normal, QPalette::ColorRole::Highlight, Qt::gray);
        option->palette.setBrush(QPalette::ColorGroup::Normal, QPalette::ColorRole::HighlightedText, normalText);

        QString player_name = index.data().toString();
        Role role = PlayerDisciplinePool::get(player_name);

        QString role_string;

        switch(role)
        {
            case Tank:  role_string = "(Tank)"; break;
            case Heal:  role_string = "(Heal)"; break;
            case Dps:   role_string = "(DPS) ";  break;
            default:    role_string = "(?)   ";    break;
        }

        option->backgroundBrush = PlayerColorPool::get(player_name);

        option->text = role_string + " " + player_name;
    }
}

void CombatTableDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    int col = index.column();

    QString player_name;
    QStyleOptionViewItem o = option;
    QStyleOptionProgressBar progressBarOption;

    int progress;

    switch(col)
    {
        case NAME_COL:
            QStyledItemDelegate::paint(painter, o, index);
            break;

        case DPS_COL:
        case HPS_COL:
            o.displayAlignment = Qt::AlignCenter;
            QStyledItemDelegate::paint(painter, o, index);
            break;

        case DAMAGE_COL:
        case HEAL_COL:
        case APM_COL:

            player_name = index.siblingAtColumn(0).data().toString();

            if (col == APM_COL)
            {
                progress = index.data().toDouble() * 1000;
            }
            else
            {
                progress = index.data().toInt();
            }

            progressBarOption.rect = option.rect;
            progressBarOption.minimum = 0;

            if (col == DAMAGE_COL)  progressBarOption.maximum = max_damage;
            if (col == HEAL_COL)    progressBarOption.maximum = max_heal;
            if (col == APM_COL)     progressBarOption.maximum = max_apm * 1000;

            progressBarOption.progress = progress;

            if (col == APM_COL)
            {
                progressBarOption.text = NumberUtils::human_readable(index.data().toFloat());
            }
            else
            {
                progressBarOption.text = NumberUtils::human_readable(index.data().toULongLong());
            }

            progressBarOption.textVisible = true;
            progressBarOption.textAlignment = Qt::AlignCenter;

            progressBarOption.palette.setColor(QPalette::ColorRole::Highlight, PlayerColorPool::get(player_name));

            QApplication::style()->drawControl(QStyle::CE_ProgressBar, &progressBarOption, painter);

            break;

        default: break;
    }

}
