#ifndef COMBATTABLEDELEGATE_H
#define COMBATTABLEDELEGATE_H

#include <QStyledItemDelegate>
#include <QPainter>
#include <QStyleOptionViewItem>
#include <QModelIndex>

enum
{
    NAME_COL    = 0,
    DAMAGE_COL  = 1,
    DPS_COL     = 2,
    HEAL_COL    = 3,
    HPS_COL     = 4,
    APM_COL     = 5
};

class CombatTableDelegate: public QStyledItemDelegate
{
public:

    CombatTableDelegate(quint64 max_damage, quint64 max_heal, qreal max_apm): max_damage(max_damage), max_heal(max_heal), max_apm(max_apm){}
    void SetValues(quint64, quint64, qreal);

    quint64 max_damage;
    quint64 max_heal;
    qreal max_apm;

    virtual void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const Q_DECL_OVERRIDE;
    virtual void initStyleOption(QStyleOptionViewItem *option, const QModelIndex &index) const Q_DECL_OVERRIDE;
};

#endif // COMBATTABLEDELEGATE_H
