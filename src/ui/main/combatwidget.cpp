#include "src/ui/main/combatwidget.h"
#include "ui_combatwidget.h"
#include <QVariant>


CombatWidget::CombatWidget(Combat* combat, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CombatWidget),
    combat(combat)
{
    ui->setupUi(this);
    ui->data->setColumnCount(6);
    ui->data->setHorizontalHeaderLabels({"Name", "Damage", "DPS", "Heal", "HPS", "APM"});
    ui->data->setSortingEnabled(true);
    ui->data->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
    ui->data->setAlternatingRowColors(true);

    cd = new CombatTableDelegate(0, 0, 0);
    ui->data->setItemDelegate(cd);

    connect(ui->data->horizontalHeader(), &QHeaderView::sectionClicked, this, [=](int col){
        sorted_column = col;
        sort_order = ui->data->horizontalHeader()->sortIndicatorOrder();
    });

    Update();
}

void CombatWidget::Update()
{
    ui->targets->setText(combat->targetNames());
    ui->time->setText(combat->displayTime());

    ui->data->setSortingEnabled(false);

    int players = combat->stats.count();
    ui->data->setRowCount(players);

    unsigned int i = 0;

    auto model = ui->data->model();

    quint64 max_damage = 0;
    quint64 max_heal = 0;
    qreal max_apm = 0;

    for (const auto& it : combat->stats.toStdMap())
    {
        double apm = (1.0*it.second.abilities / combat->duration ) * 1000;

        max_damage = std::max(it.second.damages, max_damage);
        max_heal = std::max(it.second.heals, max_heal);
        max_apm = std::max(apm, max_apm);

        model->setData(model->index(i, NAME_COL), it.first);
        model->setData(model->index(i, DAMAGE_COL), it.second.damages);
        model->setData(model->index(i, DPS_COL), (1.0*it.second.damages / combat->duration) * 1000);
        model->setData(model->index(i, HEAL_COL), it.second.heals);
        model->setData(model->index(i, HPS_COL), (1.0*it.second.heals / combat->duration ) * 1000);
        model->setData(model->index(i, APM_COL), apm);

        i++;
    }

    cd ->SetValues(max_damage, max_heal, max_apm);

    ui->data->sortItems(sorted_column, sort_order);
    ui->data->setSortingEnabled(true);
}

CombatWidget::~CombatWidget()
{
    delete ui;
}
