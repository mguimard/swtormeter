#ifndef COMBATWIDGET_H
#define COMBATWIDGET_H

#include <QWidget>
#include "src/entity/combat.h"
#include "src/ui/main/combattabledelegate.h"

namespace Ui {
class CombatWidget;
}

class CombatWidget : public QWidget
{
    Q_OBJECT

public:
    explicit CombatWidget(Combat* combat, QWidget *parent = nullptr);
    ~CombatWidget();

    void Update();

private:
    Ui::CombatWidget *ui;
    Combat* combat = nullptr;
    CombatTableDelegate* cd = nullptr;

    Qt::SortOrder sort_order = Qt::DescendingOrder;
    int sorted_column = 1;
};

#endif // COMBATWIDGET_H
