#include <QMenu>
#include <QFileDialog>
#include <QDebug>
#include <QLabel>
#include <QStyleFactory>
#include <QApplication>
#include <QPalette>
#include <QDir>
#include <QProcess>
#include <QWebSocket>

#include "ui_mainwindow.h"
#include "src/parser/oneshotparserthread.h"
#include "src/ui/main/combatlistwidget.h"
#include "src/ui/main/combatwidget.h"
#include "src/ui/main/mainwindow.h"
#include "src/util/settings.h"
#include "src/util/logger.h"

Q_DECLARE_METATYPE(Combat*)
Q_DECLARE_METATYPE(Event)
Q_DECLARE_METATYPE(QList<Event>)
Q_DECLARE_METATYPE(QList<Combat*>)

#define SERVER_PORT 12345

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    qRegisterMetaType<Combat*>();
    qRegisterMetaType<Event>();
    qRegisterMetaType<QList<Event>>();
    qRegisterMetaType<QList<Combat*>>();

    ui->setupUi(this);

    InitSocket();
    SetMenu();

    overlay_config_widget = new OverlayConfigWidget(this);

    connect(overlay_config_widget, &OverlayConfigWidget::changed, this, [=](OverlayConfig cfg){
        logger::log(typeid(this).name(), "config changed");

        if(client_socket)
        {
            client_socket->sendTextMessage(cfg.toJSON().toJson(QJsonDocument::Compact));
        }
    });
}

MainWindow::~MainWindow()
{
    if(overlay)
    {
        overlay->terminate();
        overlay->waitForFinished();
    }

    delete ui;
}

void MainWindow::InitSocket()
{
    ws = new QWebSocketServer(QStringLiteral("SwtorMeter server"), QWebSocketServer::NonSecureMode, this);

    if (ws->listen(QHostAddress::LocalHost, SERVER_PORT))
    {
        logger::log(typeid(this).name(), "Server started");

        connect(ws, &QWebSocketServer::newConnection, this, [=](){
            logger::log(typeid(this).name(), "client connected");
            client_socket = ws->nextPendingConnection();
            connect(client_socket, &QWebSocket::textMessageReceived, this, [=](QString msg){
                logger::log(typeid(this).name(), "message from client: " + msg);
            });
        });
    }
    else
    {
        logger::log(typeid(this).name(), "Server did not start");
    }
}

void MainWindow::SetMenu()
{
    QMenu* file_menu = new QMenu("File");
    ui->menubar->addMenu(file_menu);

    start_live_action = new QAction("Start parsing");
    file_menu->addAction(start_live_action);

    open_log_action = new QAction("Open log file...");
    file_menu->addAction(open_log_action);

    QObject::connect(start_live_action, &QAction::triggered, this, [&](){

        if(!rt_started){
            QString folder_path = Settings::GetCombatLogsFolder();

            if(folder_path.isEmpty())
            {
                folder_path = QFileDialog::getExistingDirectory(this, tr("Open Directory"), Settings::GetCombatLogsFolder(), QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
            }

            if(!folder_path.isEmpty()){

                Clear();

                t = new RealTimeParserThread(folder_path);

                connect(t, &RealTimeParserThread::combats_parsed, this, &MainWindow::HandleResults);
                connect(t, &RealTimeParserThread::combat_start, this, &MainWindow::NewCombatStart);
                connect(t, &RealTimeParserThread::combat_events, this, &MainWindow::NewCombatEvents);
                connect(t, &RealTimeParserThread::combat_end, this, &MainWindow::NewCombatEnd);
                connect(t, &RealTimeParserThread::finished, t, &QObject::deleteLater);

                t->start();
                rt_started = true;
                start_live_action->setText("Stop parsing");
                open_log_action->setEnabled(false);
                ui->statusbar->showMessage("Real time parsing started...");
            }
        }
        else
        {
            t->Stop();
            rt_started = false;
            start_live_action->setText("Start parsing");
            open_log_action->setEnabled(true);
            ui->statusbar->showMessage("Real time parsing stopped...");
        }
    });

    QObject::connect(open_log_action, &QAction::triggered, this, [=](){
        QString file_name = QFileDialog::getOpenFileName(this, tr("Open Combat Log"), QDir::homePath(), tr("Txt Files (*.txt)"));

        if(!file_name.isEmpty()){

            ui->statusbar->showMessage("Parsing file...");
            setEnabled(false);
            OneShotParserThread *t = new OneShotParserThread(file_name);

            connect(t, &OneShotParserThread::resultReady, this, &MainWindow::HandleResults);
            connect(t, &OneShotParserThread::finished, t, &QObject::deleteLater);

            t->start();
        }
    });

    QMenu* settings_menu = new QMenu("Settings");
    ui->menubar->addMenu(settings_menu);

    QAction* set_combat_log_path = new QAction("Set combat logs path");
    settings_menu->addAction(set_combat_log_path);

    QObject::connect(set_combat_log_path, &QAction::triggered, this, [=](){
        QString folder_path = QFileDialog::getExistingDirectory(this, tr("Open Directory"), QDir::homePath(), QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);

        if(!folder_path.isEmpty()){
            Settings::SetCombatLogsFolder(folder_path);
        }
    });

    QMenu* overlay_menu = new QMenu("Overlay");
    ui->menubar->addMenu(overlay_menu);

    QAction* toggle_overlay = new QAction("Toggle overlay");
    overlay_menu->addAction(toggle_overlay);
    toggle_overlay->setCheckable(true);

    QObject::connect(toggle_overlay, &QAction::toggled, this, [=](int state){
        if(overlay)
        {
            overlay->terminate();
            overlay->waitForFinished();
            delete overlay;
            overlay = nullptr;
        }

        if(state)
        {
            QStringList arguments = QCoreApplication::arguments();
            QString program = arguments.at(0);
            overlay = new QProcess();
            overlay->setProgram(program);
            overlay->setArguments({"overlay"});
            overlay->setProcessChannelMode(QProcess::ForwardedOutputChannel);
            overlay->start();
            overlay->waitForStarted();
        }
    });

    QAction* config_overlay = new QAction("Configure");
    overlay_menu->addAction(config_overlay);

    QObject::connect(config_overlay, &QAction::triggered, this, [=](){
        overlay_config_widget->exec();
    });
}

void MainWindow::on_combat_list_currentRowChanged(int row)
{
    ui->combat_pages->setCurrentIndex(row);
}

void MainWindow::CreateNewCombatWidgets(Combat* c)
{
    QListWidgetItem* item = new QListWidgetItem(ui->combat_list);
    CombatListWidget* clw = new CombatListWidget(c, this);
    CombatWidget* cw = new CombatWidget(c);

    item->setSizeHint(clw->sizeHint());
    ui->combat_list->addItem(item);
    ui->combat_list->setItemWidget(item, clw);
    ui->combat_pages->addWidget(cw);

    combats_widgets.push_back(cw);
    combats_list_widgets.push_back(clw);
    combats.push_back(c);

    // Auto select latest
    ui->combat_list->setCurrentRow(ui->combat_list->count() -1);

    current_live_combat = c;
    current_live_combat_widget = cw;
    current_live_combat_list_widget = clw;
}

void MainWindow::UpdateCombatWidgets()
{
    if(!current_live_combat)
    {
        logger::log(typeid(this).name(), "New combat events, but there is no active combat");
    }

    if(current_live_combat && current_live_combat_widget && current_live_combat_list_widget)
    {
        current_live_combat_widget->Update();
        current_live_combat_list_widget->Update();
    }
}

void MainWindow::UpdateOverlay()
{
    if(client_socket && current_live_combat)
    {
        QByteArray data = current_live_combat->toJSON().toJson(QJsonDocument::Compact);
        logger::log(typeid(this).name(), "ws send: " + data);
        client_socket->sendTextMessage(data);
    }
}

// SLOTS
void MainWindow::HandleResults(const QList<Combat*> data)
{
    Clear();

    for(Combat* c: data)
    {
        CreateNewCombatWidgets(c);
    }

    setEnabled(true);

    ui->statusbar->showMessage(QString::number(combats.size()) + " combats parsed.");
}

void MainWindow::NewCombatStart(Combat* c)
{
    rt_mutex.lock();    

    logger::log(typeid(this).name(), "NewCombatStart");

    if(current_live_combat)
    {
        logger::log(typeid(this).name(), "New combat started, but an other one is currently active.");
    }

    ui->statusbar->showMessage("Parsing combat...");

    UpdateOverlay();
    CreateNewCombatWidgets(c);

    rt_mutex.unlock();
}

void MainWindow::NewCombatEvents()
{
    logger::log(typeid(this).name(), "NewCombatEvents");

    rt_mutex.lock();

    UpdateOverlay();
    UpdateCombatWidgets();

    rt_mutex.unlock();
}

void MainWindow::NewCombatEnd()
{
    logger::log(typeid(this).name(), "NewCombatEnd");

    rt_mutex.lock();

    UpdateOverlay();
    UpdateCombatWidgets();

    ui->statusbar->showMessage("Waiting for a new combat...");

    current_live_combat = nullptr;
    current_live_combat_widget = nullptr;
    current_live_combat_list_widget = nullptr;

    rt_mutex.unlock();
}

void MainWindow::Clear()
{
    while(ui->combat_list->count() > 0)
    {
        QListWidgetItem* widget = ui->combat_list->takeItem(0);
        ui->combat_list->removeItemWidget(widget);
    }

    while(ui->combat_pages->count() > 0)
    {
        QWidget* widget = ui->combat_pages->widget(0);
        ui->combat_pages->removeWidget(widget);
    }

    for(CombatWidget* combat_widget: combats_widgets)
    {
        delete combat_widget;
    }

    for(CombatListWidget* combat_list_widget: combats_list_widgets)
    {
        delete combat_list_widget;
    }

    for(Combat* combat: combats)
    {
        delete combat;
    }

    combats_list_widgets.clear();
    combats_widgets.clear();
    combats.clear();
}
