#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QAction>
#include <QMutex>
#include <QProcess>
#include <QWebSocketServer>

#include "src/entity/combat.h"
#include "src/ui/main/combatwidget.h"
#include "src/ui/main/combatlistwidget.h"
#include "src/ui/main/overlayconfigwidget.h"
#include "src/parser/realtimeparserthread.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QAction* start_live_action;
    QAction* open_log_action;

    void InitSocket();
    void SetMenu();
    void Clear();
    void CreateNewCombatWidgets(Combat*);
    void UpdateCombatWidgets();
    void UpdateOverlay();

    // Slots for thread events
    void HandleResults(const QList<Combat*>);
    void NewCombatStart(Combat*);
    void NewCombatEvents();
    void NewCombatEnd();

    Combat* current_live_combat = nullptr;
    CombatWidget* current_live_combat_widget = nullptr;
    CombatListWidget* current_live_combat_list_widget = nullptr;

    bool rt_started = false;
    RealTimeParserThread *t;

    QList<Combat*> combats;
    QList<CombatWidget*> combats_widgets;
    QList<CombatListWidget*> combats_list_widgets;

    QMutex rt_mutex;

    QProcess* overlay = nullptr;
    QWebSocketServer *ws = nullptr;
    QWebSocket *client_socket = nullptr;

    OverlayConfigWidget* overlay_config_widget = nullptr;

private slots:
    void on_combat_list_currentRowChanged(int);
};
#endif // MAINWINDOW_H
