#include "overlayconfigwidget.h"
#include "ui_overlayconfigwidget.h"
#include "src/util/settings.h"

OverlayConfigWidget::OverlayConfigWidget(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::OverlayConfigWidget)
{
    ui->setupUi(this);

    cfg = Settings::GetOverlayConfig();

    ui->w->setValue(cfg.w);
    ui->h->setValue(cfg.h);
    ui->x->setValue(cfg.x);
    ui->y->setValue(cfg.y);

    connect(this, &OverlayConfigWidget::changed, [=](){
        Settings::SetOverlayConfig(cfg);
    });
}

OverlayConfigWidget::~OverlayConfigWidget()
{
    delete ui;
}

void OverlayConfigWidget::on_w_valueChanged(int value)
{
    cfg.w = value;
    emit changed(cfg);
}

void OverlayConfigWidget::on_h_valueChanged(int value)
{
    cfg.h = value;
    emit changed(cfg);
}

void OverlayConfigWidget::on_x_valueChanged(int value)
{
    cfg.x = value;
    emit changed(cfg);
}

void OverlayConfigWidget::on_y_valueChanged(int value)
{
    cfg.y = value;
    emit changed(cfg);
}
