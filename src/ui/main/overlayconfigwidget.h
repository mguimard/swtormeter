#ifndef OVERLAYCONFIGWIDGET_H
#define OVERLAYCONFIGWIDGET_H

#include <QDialog>
#include "src/util/overlay_config.h"

namespace Ui {
class OverlayConfigWidget;
}

class OverlayConfigWidget : public QDialog
{
    Q_OBJECT

public:
    explicit OverlayConfigWidget(QWidget *parent = nullptr);
    ~OverlayConfigWidget();

signals:
    void changed(OverlayConfig);

private slots:
    void on_w_valueChanged(int);
    void on_h_valueChanged(int);
    void on_x_valueChanged(int);
    void on_y_valueChanged(int);

private:
    Ui::OverlayConfigWidget *ui;
    OverlayConfig cfg;
};

#endif // OVERLAYCONFIGWIDGET_H
