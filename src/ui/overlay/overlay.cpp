#include "overlay.h"
#include "ui_overlay.h"
#include <iostream>
#include "src/util/logger.h"
#include <QFontDatabase>
#include <QFile>

Overlay::Overlay(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Overlay)
{
    QFontDatabase::addApplicationFont(":/SWEuroRegular.ttf");
    QFontDatabase::addApplicationFont(":/RestoreBD.ttf");

    QFile styleFile(":/overlay.qss");
    styleFile.open(QFile::ReadOnly);
    QString style(styleFile.readAll());
    setStyleSheet(style);

    ui->setupUi(this);

    UpdateModeDisplay();

    cd = new OverlayTableDelegate(0);
    ui->data->setItemDelegate(cd);
    ui->data->setColumnCount(2);
    ui->data->horizontalHeader()->hide();
    ui->data->verticalHeader()->hide();
    ui->data->setShowGrid(false);
    ui->data->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->data->setSelectionMode(QAbstractItemView::NoSelection);
}

void Overlay::on_next_mode_clicked()
{
    current_overlay_mode++;

    if(current_overlay_mode >= OVERLAY_MODES_COUNT)
    {
        current_overlay_mode = 0;
    }

    UpdateModeDisplay();
}

void Overlay::on_previous_mode_clicked()
{
    current_overlay_mode--;

    if(current_overlay_mode < 0)
    {
        current_overlay_mode = OVERLAY_MODES_COUNT - 1;
    }

    UpdateModeDisplay();
}

void Overlay::UpdateModeDisplay()
{
    QString title;

    switch(current_overlay_mode)
    {
    case OVERLAY_MODE_DPS: title = "DPS"; break;
    case OVERLAY_MODE_HPS: title = "HPS"; break;
    case OVERLAY_MODE_APM: title = "APM"; break;
    default: break;
    }

    ui->mode->setText(title);

    if(combat)
    {
        logger::log(typeid(this).name(),"Combat not empty");
        switch(current_overlay_mode)
        {
        case OVERLAY_MODE_DPS: DrawFPSMode(); break;
        case OVERLAY_MODE_HPS: DrawHPSMode(); break;
        case OVERLAY_MODE_APM: DrawAPMMode(); break;
        default: break;
        }
    }
    else
    {
        logger::log(typeid(this).name(),"Combat is empty");
        ui->data->clear();
        ui->data->clearContents();
        ui->data->setRowCount(0);
    }
}

void Overlay::OnCombatData(Combat* c)
{
    if(combat)
    {
        delete combat;
        combat = nullptr;
    }

    combat = c;

    switch(current_overlay_mode)
    {
    case OVERLAY_MODE_DPS: DrawFPSMode(); break;
    case OVERLAY_MODE_HPS: DrawHPSMode(); break;
    case OVERLAY_MODE_APM: DrawAPMMode(); break;
    default: break;
    }
}

void Overlay::DrawFPSMode()
{
    ui->data->setRowCount(combat->stats.count());

    auto model = ui->data->model();

    quint8 i = 0;
    quint64 max_dps = 0;

    for (const auto& it : combat->stats.toStdMap())
    {
        float dps = (1.0*it.second.damages / combat->duration) * 1000;
        max_dps = dps > max_dps ? dps : max_dps;
        model->setData(model->index(i, 0), it.first);
        model->setData(model->index(i, 1), dps);
        i++;
    }

    cd->SetMaxValue(max_dps);
    ui->data->resizeColumnsToContents();
    ui->data->horizontalHeader()->setStretchLastSection(true);

    ui->data->sortItems(1, Qt::DescendingOrder);
}

void Overlay::DrawHPSMode()
{
    ui->data->setRowCount(combat->stats.count());

    auto model = ui->data->model();

    quint8 i = 0;
    quint64 max_hps = 0;

    for (const auto& it : combat->stats.toStdMap())
    {
        float hps = (1.0*it.second.heals / combat->duration) * 1000;
        max_hps = hps > max_hps ? hps : max_hps;
        model->setData(model->index(i, 0), it.first);
        model->setData(model->index(i, 1), hps);
        i++;
    }

    cd->SetMaxValue(max_hps);
    ui->data->resizeColumnsToContents();
    ui->data->horizontalHeader()->setStretchLastSection(true);

    ui->data->sortItems(1, Qt::DescendingOrder);
}

void Overlay::DrawAPMMode()
{
    ui->data->setRowCount(combat->stats.count());

    auto model = ui->data->model();

    unsigned int i = 0;
    double max_apm = 0;

    for (const auto& it : combat->stats.toStdMap())
    {
        float apm = (1.0*it.second.abilities / combat->duration) * 1000;
        max_apm = apm > max_apm ? apm : max_apm;
        model->setData(model->index(i, 0), it.first);
        model->setData(model->index(i, 1), apm);
        i++;
    }

    cd->SetMaxValue(max_apm);
    ui->data->resizeColumnsToContents();
    ui->data->horizontalHeader()->setStretchLastSection(true);
    ui->data->sortItems(1, Qt::DescendingOrder);
}

Overlay::~Overlay()
{
    delete ui;
}
