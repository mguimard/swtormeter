#ifndef OVERLAY_H
#define OVERLAY_H

#include <QWidget>
#include "src/ui/overlay/overlaytabledelegate.h"
#include "src/entity/combat.h"

namespace Ui {
class Overlay;
}

#define OVERLAY_MODES_COUNT 3
#define OVERLAY_MODES_COUNT 3

enum
{
    OVERLAY_MODE_DPS = 0,
    OVERLAY_MODE_HPS = 1,
    OVERLAY_MODE_APM = 2
};

class Overlay : public QWidget
{
    Q_OBJECT

public:
    explicit Overlay(QWidget *parent = nullptr);
    ~Overlay();
    void OnCombatData(Combat*);

private slots:
    void on_previous_mode_clicked();
    void on_next_mode_clicked();

private:
    Ui::Overlay *ui;
    OverlayTableDelegate *cd;
    Combat* combat = nullptr;

    void DrawFPSMode();
    void DrawHPSMode();
    void DrawAPMMode();

    void UpdateModeDisplay();

    char current_overlay_mode = OVERLAY_MODE_DPS;
};

#endif // OVERLAY_H
