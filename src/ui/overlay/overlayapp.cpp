#include "overlayapp.h"
#include "src/util/logger.h"
#include "src/util/overlay_config.h"
#include "src/util/settings.h"

#include <QDebug>
#include <QWebSocket>

#ifdef __linux__
#include <LayerShellQt/Shell>
#include <LayerShellQt/Window>
#elif _WIN32
#include <windows.h>
#pragma comment(lib,"user32.lib")
#else
// not supported
#endif

OverlayApp::OverlayApp()
{
    w = new Overlay(nullptr);
}

void OverlayApp::run()
{    
    cfg = Settings::GetOverlayConfig();
    InitSocket();
    InitGUI();
}

void OverlayApp::InitSocket()
{
    QWebSocket* ws = new QWebSocket();

    connect(ws, QOverload<QAbstractSocket::SocketError>::of(&QWebSocket::error),[=](QAbstractSocket::SocketError error){
        logger::log(typeid(this).name(), "ws error: " + QString::number(error) + " " + ws->errorString());
    });

    connect(ws, &QWebSocket::connected, this, [=](){
        logger::log(typeid(this).name(), "ws client connected");
    });

    connect(ws, &QWebSocket::textMessageReceived, this, &OverlayApp::OnTextMessage);

    logger::log(typeid(this).name(), "opening ws");

    ws->open(QUrl(QStringLiteral("ws://localhost:12345")));
}

void OverlayApp::InitGUI()
{

#ifdef __linux__
    LayerShellQt::Shell::useLayerShell();

    w->resize(cfg.w, cfg.h);
    w->show();
    w->hide();

    QWindow *lwin = w->windowHandle();

    LayerShellQt::Window::Anchors anchors;
    anchors.setFlag(LayerShellQt::Window::AnchorTop);
    anchors.setFlag(LayerShellQt::Window::AnchorLeft);

    QMargins margins;
    margins.setTop(cfg.x);
    margins.setLeft(cfg.y);

    if (LayerShellQt::Window *lsh = LayerShellQt::Window::get(lwin))
    {
        lsh->setScope("SwtorMeter");
        lsh->setLayer(LayerShellQt::Window::LayerOverlay);
        lsh->setAnchors(anchors);
        lsh->setMargins(margins);
        lsh->setKeyboardInteractivity(LayerShellQt::Window::KeyboardInteractivityNone);
    }
    else
    {
        logger::log("main", "LayerShellQt::Window not found");
    }

    w->show();

#elif _WIN32
    w->setWindowFlags(Qt::CustomizeWindowHint | Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);

    w->setAttribute(Qt::WA_NoSystemBackground);
    w->setAttribute(Qt::WA_TranslucentBackground);
    w->setAttribute(Qt::WA_TransparentForMouseEvents);

    HWND hWnd = (HWND) w->winId();

    int style = GetWindowLong(hWnd, GWL_EXSTYLE);
    style |= WS_EX_TOOLWINDOW;
    style |= WS_EX_NOACTIVATE;
    style &= ~WS_EX_APPWINDOW;
    style &= ~WS_EX_TRANSPARENT;
    style &= ~WS_EX_LAYERED;
    style |= WS_EX_TOOLWINDOW;

    ShowWindow(hWnd, SW_HIDE);
    SetWindowLong(hWnd, GWL_EXSTYLE, style);
    ShowWindow(hWnd, SW_SHOW);

    w->show();
    SetWindowPos(hWnd, HWND_TOPMOST, cfg.x, cfg.y, cfg.w, cfg.h, SWP_SHOWWINDOW | SWP_NOACTIVATE | SWP_NOREDRAW);

#else
    // not supported
#endif
}

void OverlayApp::OnTextMessage(QString msg)
{
    logger::log(typeid(this).name(), "msg from server: " + msg);
    QJsonDocument doc = QJsonDocument::fromJson(msg.toUtf8());

    if(doc.object().contains("stats"))
    {
        w->OnCombatData(Combat::fromJSON(doc));

#ifdef _WIN32
        // bring to top on new combat events in case swtor runs in fullscreen mode not windowed
        HWND hWnd = (HWND) w->winId();
        SetWindowPos(hWnd, HWND_TOPMOST, cfg.x, cfg.y, cfg.w, cfg.h, SWP_SHOWWINDOW | SWP_NOACTIVATE | SWP_NOREDRAW);
#endif
    }

    if(doc.object().contains(OVERLAY_CONFIG_KEY))
    {
        cfg = OverlayConfig::fromJSON(doc);

#ifdef __linux__
        w->hide();

        if (LayerShellQt::Window *lsh = LayerShellQt::Window::get(w->windowHandle()))
        {
            LayerShellQt::Window::Anchors anchors;
            anchors.setFlag(LayerShellQt::Window::AnchorTop);
            anchors.setFlag(LayerShellQt::Window::AnchorLeft);

            QMargins margins;
            margins.setTop(cfg.x);
            margins.setLeft(cfg.y);

            lsh->setAnchors(anchors);
            lsh->setMargins(margins);
        }
        else
        {
            logger::log(typeid(this).name(),"No handle available");
        }

        w->resize(cfg.w, cfg.h);
        w->show();
#elif _WIN32
        HWND hWnd = (HWND) w->winId();
        SetWindowPos(hWnd, HWND_TOPMOST, cfg.x, cfg.y, cfg.w, cfg.h, SWP_SHOWWINDOW | SWP_NOACTIVATE | SWP_NOREDRAW);
#else
        // not supported
#endif
    }
}
