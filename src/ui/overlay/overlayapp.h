#ifndef OVERLAYAPP_H
#define OVERLAYAPP_H

#include "src/ui/overlay/overlay.h"
#include "src/util/overlay_config.h"

class OverlayApp : public QObject
{
public:
    OverlayApp();
    void run();

private slots:
    void OnTextMessage(QString);

private:
    Overlay* w;
    void InitSocket();
    void InitGUI();
    OverlayConfig cfg;
};

#endif // OVERLAYAPP_H
