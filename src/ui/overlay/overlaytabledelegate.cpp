#include "overlaytabledelegate.h"
#include "src/entity/playercolorpool.h"

#include <QApplication>
#include <QStyleOption>

void OverlayTableDelegate::SetMaxValue(double v)
{
    max_value = v;
}

void OverlayTableDelegate::initStyleOption(QStyleOptionViewItem *option, const QModelIndex &index) const
{
    QStyledItemDelegate::initStyleOption(option, index);

    int col = index.column();

    if(col == 0)
    {
        QBrush normalText = option->palette.brush(QPalette::ColorGroup::Normal, QPalette::ColorRole::Text);
        normalText.setColor(QColor("#ded199"));

        option->palette.setBrush(QPalette::ColorGroup::Normal, QPalette::ColorRole::Highlight, Qt::gray);
        option->palette.setBrush(QPalette::ColorGroup::Normal, QPalette::ColorRole::HighlightedText, normalText);

        QString player_name = index.data().toString();
        option->backgroundBrush = PlayerColorPool::get(player_name, 127);

        option->text = player_name;
    }
}

void OverlayTableDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    int col = index.column();

    QString player_name;
    QStyleOptionViewItem o = option;
    QStyleOptionProgressBar progressBarOption;

    float progress;

    switch(col)
    {
    case 0:
        QStyledItemDelegate::paint(painter, o, index);
        break;

    case 1:

        painter->save();

        player_name = index.siblingAtColumn(0).data().toString();

        progress = max_value > 0 ? index.data().toDouble() / max_value : 0;

        painter->fillRect(QRect(option.rect.left(), option.rect.top(),
                                option.rect.width() * progress, option.rect.height()),
                                QBrush(PlayerColorPool::get(player_name, 127)));

        painter->restore();

        o.displayAlignment = Qt::AlignRight | Qt::AlignVCenter;
        o.backgroundBrush = QBrush(Qt::NoBrush);

        QStyledItemDelegate::paint(painter, o, index);

        break;

    default: break;
    }

}
