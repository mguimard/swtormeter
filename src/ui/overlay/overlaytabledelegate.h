#ifndef OVERLAYTABLEDELEGATE_H
#define OVERLAYTABLEDELEGATE_H
#include <QStyledItemDelegate>
#include <QPainter>
#include <QStyleOptionViewItem>
#include <QModelIndex>

class OverlayTableDelegate: public QStyledItemDelegate
{
public:

    OverlayTableDelegate(double max_value): max_value(max_value){}
    void SetMaxValue(double);

    double max_value;

    virtual void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const Q_DECL_OVERRIDE;
    virtual void initStyleOption(QStyleOptionViewItem *option, const QModelIndex &index) const Q_DECL_OVERRIDE;
};

#endif // OVERLAYTABLEDELEGATE_H
