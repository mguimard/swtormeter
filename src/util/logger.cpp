#include "src/util/logger.h"
#include <QTime>

void logger::log(QString tag, QString message)
{
    QTime now = QTime::currentTime();

    printf("[%d:%d:%d +%d] [%s] %s\n",
           now.hour(),
           now.minute(),
           now.second(),
           now.msec(),
           tag.toStdString().c_str(),
           message.toStdString().c_str());
}
