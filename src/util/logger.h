#ifndef LOGGER_H
#define LOGGER_H

#include <QString>

class logger
{
public:
    static void log(QString, QString);

private:
    logger(){};
};

#endif // LOGGER_H
