#ifndef NUMBERUTILS_H
#define NUMBERUTILS_H

#include <QString>

class NumberUtils
{
public:
    static QString human_readable(double value)
    {
        if(value > 1000)
        {
            value /= 1000;
            return QString::number(value, 'f', 2) + "k";
        }

        return QString::number(value, 'f', 2);
    }
private:
    NumberUtils();
};

#endif // NUMBERUTILS_H
