#ifndef OVERLAY_CONFIG_H
#define OVERLAY_CONFIG_H

#include <QJsonDocument>
#include <QJsonObject>

#include "src/util/logger.h"

#define OVERLAY_CONFIG_KEY "overlay_config"
#define OVERLAY_MIN_WIDTH 320
#define OVERLAY_MIN_HEIGHT 280

struct OverlayConfig
{
    int w = OVERLAY_MIN_WIDTH;
    int h = OVERLAY_MIN_HEIGHT;
    int x = 0;
    int y = 0;

    QJsonDocument toJSON() const
    {
        QJsonObject o;

        o["w"] = w;
        o["h"] = h;        
        o["x"] = x;
        o["y"] = y;

        QJsonObject config_object;
        config_object[OVERLAY_CONFIG_KEY]  = o;

        return QJsonDocument(config_object);
    }

    static OverlayConfig fromJSON(QJsonDocument doc)
    {
        QJsonObject o = doc.object()[OVERLAY_CONFIG_KEY].toObject();

        OverlayConfig cfg;

        cfg.w = o["w"].toInt();
        cfg.h = o["h"].toInt();
        cfg.x = o["x"].toInt();
        cfg.y = o["y"].toInt();       

        return cfg;
    }
};

#endif // OVERLAY_CONFIG_H
