#include "src/util/settings.h"
#include <QDir>

Settings* Settings::instance;

Settings::Settings(){}

QString Settings::GetCombatLogsFolder()
{
    if(!instance)
    {
        instance = new Settings();
    }

    return instance->store.value(COMBAT_LOGS_FOLDER_KEY, "").toString();
}

void Settings::SetCombatLogsFolder(QString folder)
{
    if(!instance)
    {
        instance = new Settings();
    }

    instance->store.setValue(COMBAT_LOGS_FOLDER_KEY, folder);
}

OverlayConfig Settings::GetOverlayConfig()
{
    if(!instance)
    {
        instance = new Settings();
    }

    OverlayConfig cfg;

    cfg.w = instance->store.value("overlay/w", OVERLAY_MIN_WIDTH).toInt();
    cfg.h = instance->store.value("overlay/h", OVERLAY_MIN_HEIGHT).toInt();
    cfg.x = instance->store.value("overlay/x", 0).toInt();
    cfg.y = instance->store.value("overlay/y", 0).toInt();

    return cfg;
}

void Settings::SetOverlayConfig(OverlayConfig cfg)
{
    if(!instance)
    {
        instance = new Settings();
    }

    instance->store.setValue("overlay/w", cfg.w);
    instance->store.setValue("overlay/h", cfg.h);
    instance->store.setValue("overlay/x", cfg.x);
    instance->store.setValue("overlay/y", cfg.y);
}

