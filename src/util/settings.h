#ifndef SETTINGS_H
#define SETTINGS_H

#include "src/util/overlay_config.h"
#include <QString>
#include <QSettings>

#define COMBAT_LOGS_FOLDER_KEY "combatlogs/folder"

class Settings
{
public:
    static QString GetCombatLogsFolder();
    static void SetCombatLogsFolder(QString);

    static OverlayConfig GetOverlayConfig();
    static void SetOverlayConfig(OverlayConfig);

private:
    Settings();
    static Settings* instance;
    QSettings store;
};

#endif // SETTINGS_H
